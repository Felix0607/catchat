require('dotenv').config();
const line = require('@line/bot-sdk');
const express = require('express');
const { Configuration, OpenAIApi } = require("openai");
const configuration = new Configuration({
  apiKey: process.env.CHATGPT_API_KEY,
});
const openai = new OpenAIApi(configuration);

const config = {
  channelAccessToken: process.env.LINE_CHANNEL_ACCESS_TOKEN,
  channelSecret: process.env.LINE_CHANNEL_SECRET,
};

const client = new line.Client(config);

const app = express();

app.post('/webhook', line.middleware(config), (req, res) => {
  Promise
    .all(req.body.events.map(handleEvent))
    .then((result) => res.json(result))
    .catch((err) => {
      console.error(err);
      res.status(500).end();
    });
});

app.get('/health', (req, res) => {
  res.status(200).send('OK');
});

function handleEvent (event) {
  if (event.type !== 'message' || event.message.type !== 'text') {
    return Promise.resolve(null);
  }

  return openai.createCompletion({ // 方法用錯修正
    prompt: event.message.text,
    model: "text-davinci-003", // 使用 model 而非 engine
    max_tokens: 1000 // 應用 max_tokens 而非 maxTokens
  }).then((completions) => {
    // 少了 data 這層結構，並要加上 trim() 去除空白
    const message = completions.data.choices[0].text.trim();
    return client.replyMessage(event.replyToken, {
      type: 'text',
      text: message
    });
  }).catch((err) => {
    console.error(err);
    return client.replyMessage(event.replyToken, {
      type: 'text',
      text: `系統發生錯誤：${err.response? err.response.statusText : err.message}`
    });
  });
}

app.listen(3000);
console.log('Linebot is running on 3000 port');